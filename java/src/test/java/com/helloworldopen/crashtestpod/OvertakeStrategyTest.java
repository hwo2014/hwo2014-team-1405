package com.helloworldopen.crashtestpod;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class OvertakeStrategyTest {

    private OvertakeStrategy overtakeStrategy;
    private Track track;
    private MockCar[] cars;
    private MockSwitch[] switches;
    private MockCar myCar;
    private MockCar car1;
    private MockCar car2;
    private MockCar car3;
    private int laneCount;
    private int pieceCount;

    @Before
    public void setUp() throws Exception {
        myCar = new MockCar(0, 4, 0);
        car1 = new MockCar(0, 0, 0);
        car2 = new MockCar(0, 0, 0);
        car3 = new MockCar(0, 0, 0);
        cars = new MockCar[]{myCar, car1, car2, car3};

        switches = new MockSwitch[]{
                new MockSwitch(1, true)
                , new MockSwitch(5, true)
                , new MockSwitch(10, true)
                , new MockSwitch(20, true)
                , new MockSwitch(21, true)
                , new MockSwitch(25, true)
                , new MockSwitch(30, false)
        };

        laneCount = 3;
        pieceCount = 40;

        track = new MockTrack(cars, myCar, switches, laneCount, pieceCount);
        overtakeStrategy = new OvertakeStrategy(track);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testGetBlockingCars() throws Exception {
        // on the first switch on 0 lane
        car1.lane = 0;
        car1.speed = 2.0;
        car1.index = 30;

        // between the switches 1st lane
        car2.lane = 1;
        car2.speed = 2.0;
        car2.index = 0;

        // on the second switch on 2nd lane
        car3.lane = 2;
        car3.speed = 2.0;
        car3.index = 1;

        myCar.speed = 3.0;
        myCar.lane = 1;
        myCar.index = 24;

        Car[] blockers = overtakeStrategy.getBlockingCars();

        assertEquals(car1, blockers[0]);
        assertEquals(car2, blockers[1]);
        assertEquals(car3, blockers[2]);
    }

    @Test
    public void testWrappingBetweenSwitches() throws Exception {

        car1.lane = 0;
        car1.speed = 2.0;
        car1.index = 5;

        car2.lane = 1;
        car2.speed = 2.0;
        car2.index = 8;

        car3.lane = 2;
        car3.speed = 2.0;
        car3.index = 10;

        myCar.speed = 3.0;
        myCar.lane = 1;
        myCar.index = 4;

        Car[] blockers = overtakeStrategy.getBlockingCars();

        assertEquals(car1, blockers[0]);
        assertEquals(car2, blockers[1]);
        assertEquals(car3, blockers[2]);
    }

    @Test
    public void testWrappingBeforeCar() throws Exception {

        car1.lane = 0;
        car1.speed = 2.0;
        car1.index = 0;

        car2.lane = 1;
        car2.speed = 2.0;
        car2.index = 1;

        car3.lane = 2;
        car3.speed = 2.0;
        car3.index = 2;

        myCar.speed = 3.0;
        myCar.lane = 1;
        myCar.index = 35;

        Car[] blockers = overtakeStrategy.getBlockingCars();
        // first car is before switch so we don't care
        assertEquals(null, blockers[0]);
        assertEquals(car2, blockers[1]);
        assertEquals(car3, blockers[2]);
    }

    @Test
    public void testSlowestCar() throws Exception {

        car1.lane = 0;
        car1.speed = 2.0;
        car1.index = 30;

        car2.lane = 0;
        car2.speed = 3.0;
        car2.index = 1;

        car3.lane = 0;
        car3.speed = 4.0;
        car3.index = 2;

        myCar.speed = 5.0;
        myCar.lane = 0;
        myCar.index = 24;

        Car[] blockers = overtakeStrategy.getBlockingCars();

        // can't turn left
        assertNull(blockers[0]);
        // slowest car here
        assertEquals(car1, blockers[1]);
        // nothing on right
        assertNull(blockers[2]);
    }

    @Test
    public void testNoCars() throws Exception {

        car1.lane = 0;
        car1.speed = 2.0;
        car1.index = 0;

        car2.lane = 0;
        car2.speed = 3.0;
        car2.index = 1;

        car3.lane = 0;
        car3.speed = 4.0;
        car3.index = 2;

        myCar.speed = 5.0;
        myCar.lane = 2;
        myCar.index = 21;

        Car[] blockers = overtakeStrategy.getBlockingCars();

        // nothing on left
        assertNull(blockers[0]);
        // nothing ahead
        assertNull(blockers[1]);
        // can't go right
        assertNull(blockers[2]);
    }

    @Test
    public void testAllTogether() throws Exception {

        car1.lane = 1;
        car1.speed = 2.0;
        car1.index = 0;

        car2.lane = 1;
        car2.speed = 3.0;
        car2.index = 0;

        car3.lane = 0;
        car3.speed = 4.0;
        car3.index = 0;

        myCar.speed = 5.0;
        myCar.lane = 0;
        myCar.index = 0;

        Car[] blockers = overtakeStrategy.getBlockingCars();

        // nothing on left
        assertNull(blockers[0]);
        // nothing ahead
        assertNull(blockers[1]);
        // can't go right
        assertNull(blockers[2]);
    }




    @Test
    public void testSorting() {
        int[] r;
        r = overtakeStrategy.sortCandidateIndexes(new int[]{-1, 1, 2});
        assertEquals(2, r[0]);
        assertEquals(1, r[1]);
        assertEquals(0, r[2]);
        r = overtakeStrategy.sortCandidateIndexes(new int[]{0, 20, 1});
        assertEquals(1, r[0]);
        assertEquals(2, r[1]);
        assertEquals(0, r[2]);
        r = overtakeStrategy.sortCandidateIndexes(new int[]{10, 0, 20});
        assertEquals(2, r[0]);
        assertEquals(0, r[1]);
        assertEquals(1, r[2]);
        r = overtakeStrategy.sortCandidateIndexes(new int[]{-1, 0, -2});
        assertEquals(1, r[0]);
        assertEquals(0, r[1]);
        assertEquals(2, r[2]);
        r = overtakeStrategy.sortCandidateIndexes(new int[]{2, 0, 1});
        assertEquals(0, r[0]);
        assertEquals(2, r[1]);
        assertEquals(1, r[2]);
        r = overtakeStrategy.sortCandidateIndexes(new int[]{2, 1, 0});
        assertEquals(0, r[0]);
        assertEquals(1, r[1]);
        assertEquals(2, r[2]);

        r = overtakeStrategy.sortCandidateIndexes(new int[]{2, 2, 0});
        assertTrue((r[0] == 1 && r[1] == 2) || (r[1] == 1 && r[2] == 2));
        assertEquals(2, r[2]);

        r = overtakeStrategy.sortCandidateIndexes(new int[]{0, 2, 2});
        assertTrue((r[0] == 1 && r[1] == 2) || (r[1] == 1 && r[2] == 2));
        assertEquals(0, r[2]);
    }
}


class MockCar implements Car {

    int lane;
    double speed;
    int index;

    public MockCar(int lane, double speed, int index) {
        this.lane = lane;
        this.speed = speed;
        this.index = index;
    }

    @Override
    public String getColor() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getName() {
        throw new UnsupportedOperationException();
    }

    @Override
    public double getDistanceTravelled() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getStartLane() {
        return lane;
    }

    @Override
    public int getEndLane() {
        return lane;
    }

    @Override
    public double getSlipAngle() {
        throw new UnsupportedOperationException();
    }

    @Override
    public double getAngularVelocity() {
        throw new UnsupportedOperationException();
    }

    @Override
    public double getAngularAcceleration() {
        throw new UnsupportedOperationException();
    }

    @Override
    public double getSlipVelocity() {
        throw new UnsupportedOperationException();
    }

    @Override
    public double getSlipAcceleration() {
        throw new UnsupportedOperationException();
    }

    @Override
    public double getSpeed() {
        return speed;
    }

    @Override
    public double getLeverLength() {
        throw new UnsupportedOperationException();
    }

    @Override
    public double getInPieceDistance() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getPieceIndex() {
        return index;
    }

    @Override
    public void setThrottle(double throttle) {
        throw new UnsupportedOperationException();
    }
}

class MockSwitch implements TrackPiece {

    int index;
    boolean hasSwitch;

    public MockSwitch(int index, boolean hasSwitch) {

        this.index = index;
        this.hasSwitch = hasSwitch;
    }

    @Override
    public double getLength(int startLane, int endLane) {
        throw new UnsupportedOperationException();
    }

    @Override
    public double getRadius(int startLane, int endLane) {
        throw new UnsupportedOperationException();
    }

    @Override
    public double getBendAngle() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasSwitch() {
        return hasSwitch;
    }

    @Override
    public int getIndex() {
        return index;
    }
}

class MockTrack implements Track {

    Car[] cars;
    Car myCar;
    TrackPiece[] bends;
    int laneCount;
    int pieceCount;

    public MockTrack(Car[] cars, Car myCar, TrackPiece[] bends, int laneCount, int pieceCount) {

        this.cars = cars;
        this.myCar = myCar;
        this.bends = bends;
        this.laneCount = laneCount;
        this.pieceCount = pieceCount;
    }

    @Override
    public void initMyCar(Object myCar) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void initTrack(Object trackInfo) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void updatePositions(Object positions) {
        throw new UnsupportedOperationException();
    }

    @Override
    public double myDistanceToNextBend(int laneIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public double myDistanceToNextSwitch(int laneIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public double myDistanceToSecondNextSwitch(int laneIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public double myBendRadius(int laneIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public double myNextBendRadius(int laneIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public double myBendAngle() {
        throw new UnsupportedOperationException();
    }

    @Override
    public double myNextBendAngle() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Car getMyCar() {
        return myCar;
    }

    @Override
    public int getNumberOfLanes() {
        return laneCount;
    }

    @Override
    public int getMaxDistancePieceIndex() {
        throw new UnsupportedOperationException();
    }

    @Override
    public TrackPiece[] nextPieces(int count) {
        throw new UnsupportedOperationException();
    }

    @Override
    public TrackPiece[] nextPieces(int start, int count) {
        return bends;
    }

    @Override
    public int pieceCount() {
        return pieceCount;
    }

    @Override
    public Car[] getCars() {
        return cars;
    }
}