package com.helloworldopen.crashtestpod;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import static org.junit.Assert.assertEquals;

/**
 * Created by Rene Raasuke on 15.04.2014.
 */
public class BasicTrackTest {

    private Gson gson;

    private BasicTrack track;

    @Before
    public void setUp() throws Exception {
        track = new BasicTrack();
        gson = new Gson();
    }

    @After
    public void tearDown() throws Exception {
        track = null;
        gson = null;
    }

    @Test
    public void testInitTrack() {
        String myCarMsg = "{\"msgType\":\"yourCar\",\"data\":{\"name\":\"Crash Test Pod\",\"color\":\"red\"},\"gameId\":\"d81e8e66-8ddf-4bcf-8b50-b1843d15b872\"}";
        String initMsg = "{\"msgType\":\"gameInit\",\"data\":{\"race\":{\"track\":{\"id\":\"keimola\",\"name\":\"Keimola\",\"pieces\":[{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":62.0},{\"radius\":100,\"angle\":-45.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":100.0,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":90.0}],\"lanes\":[{\"distanceFromCenter\":-10,\"index\":0},{\"distanceFromCenter\":10,\"index\":1}],\"startingPoint\":{\"position\":{\"x\":-300.0,\"y\":-44.0},\"angle\":90.0}},\"cars\":[{\"id\":{\"name\":\"Crash Test Pod\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":3,\"maxLapTimeMs\":60000,\"quickRace\":true}}},\"gameId\":\"d81e8e66-8ddf-4bcf-8b50-b1843d15b872\"}";
        track.initMyCar(gson.fromJson(myCarMsg, JsonObject.class));
        track.initTrack(gson.fromJson(initMsg, JsonObject.class));

        assertEquals("red", track.getMyCar().getColor());
        assertEquals("Crash Test Pod", track.getMyCar().getName());

        String updateMsg = "{\"msgType\":\"carPositions\",\"data\":[{\"id\":{\"name\":\"Crash Test Pod\",\"color\":\"red\"},\"angle\":0.0,\"piecePosition\":{\"pieceIndex\":0,\"inPieceDistance\":0.0,\"lane\":{\"startLaneIndex\":0,\"endLaneIndex\":0},\"lap\":0}}],\"gameId\":\"d81e8e66-8ddf-4bcf-8b50-b1843d15b872\",\"gameTick\":1}";
        track.updatePositions(gson.fromJson(updateMsg, JsonObject.class));

        InputStream dataIS = this.getClass().getClassLoader().getResourceAsStream("ServerMsgsKeimola1.txt");
        BufferedReader lineReader = new BufferedReader(new InputStreamReader(dataIS));

        lineReader.lines()
                .filter(line -> line.startsWith("{\"msgType\":\"carPositions\","))
                .forEach(line -> track.updatePositions(gson.fromJson(line, JsonObject.class)));

        assertEquals(10503, track.getMyCar().getDistanceTravelled(), 0.01);
    }


}
