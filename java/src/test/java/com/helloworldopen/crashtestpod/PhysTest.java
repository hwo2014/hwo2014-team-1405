package com.helloworldopen.crashtestpod;

import org.junit.Test;

import static com.helloworldopen.crashtestpod.Phys.*;
import static org.junit.Assert.*;

/**
 *
 */
public class PhysTest {

    public static final int SPEED = 7;
    public static final int RADIUS = 110;

    @Test
    public void testCentripetalAcceleration() throws Exception {
        assertEquals(0.3273, centripetalAcceleration(6, 110), 0.0001);
        assertEquals(0.36, centripetalAcceleration(6, 100), 0.0001);
        assertEquals(0.4, centripetalAcceleration(6, 90), 0.0001);
    }

    @Test
    public void testLogic() throws Exception {
        double a = centripetalAcceleration(SPEED, RADIUS);
        double f = force(1, a);
        double v = curveSpeed(f, RADIUS);

        assertEquals(SPEED, v, 0.1);

    }

    @Test
    public void testSlipLogic() throws Exception {
        double sideSpeed = slipSideSpeed(SPEED, 0.038);
        double slipAngle = slipAngle(SPEED, sideSpeed);
        double forwardSpeed = slipForwardSpeed(sideSpeed, 0.038);

        assertEquals(7, forwardSpeed, 0.001);
    }

    @Test
    public void testAngularCircle() throws Exception {
        double angle = 0.471234467683748;

        double dampeningCoefficient = .2;
        double angularVelocity = -0.287655323162522;
        double frictionForce = .6;
        double angularAcceleration = angularAcceleration(angle, angularVelocity, frictionForce, dampeningCoefficient);
        double calculatedDC = dampeningCoefficient(angle, angularVelocity, frictionForce, angularAcceleration);
        double calculatedFF = frictionForce(angle, angularVelocity, dampeningCoefficient, angularAcceleration);
        assertEquals(-0.214860866723574, angularAcceleration, 0.0001);
        assertEquals(dampeningCoefficient, calculatedDC, 0.0001);
        assertEquals(frictionForce, calculatedFF, 0.0001);
    }

    @Test
    public void testTicksToBrake() throws Exception {
        assertEquals(85.86, ticksToBrake(17, 3), .1);
        assertEquals(692.976, distanceTravelledWhileBraking(17, 3), .01);

    }
}

