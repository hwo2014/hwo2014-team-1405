package com.helloworldopen.crashtestpod.copilots;

import com.helloworldopen.crashtestpod.Car;
import com.helloworldopen.crashtestpod.Track;
import com.helloworldopen.crashtestpod.TrackPiece;
import org.junit.Before;
import org.junit.Test;

public class PhysicsDetectorTest {

    private MockTrack track;
    private MockCar car;

    @Before
    public void setUp() throws Exception {
        track = new MockTrack();
        car = new MockCar();
        car.inPieceDistance = 10.5103;
        car.distanceTravelled = 0.2;
        track.car = car;


    }

    @Test
    public void testSuggestThrottle() throws Exception {
        PhysicsDetector pd = new PhysicsDetector(track);
        pd.model.learn(car.getDistanceTravelled(), 0.2, 1, 0, 110, 45, 20);
    }

    private class MockCar implements Car {

        public double inPieceDistance;
        public double distanceTravelled;

        @Override
        public String getColor() {
            return null;
        }

        @Override
        public String getName() {
            return null;
        }

        @Override
        public double getDistanceTravelled() {
            return distanceTravelled;
        }

        @Override
        public int getStartLane() {
            return 0;
        }

        @Override
        public int getEndLane() {
            return 0;
        }

        @Override
        public double getSlipAngle() {
            return 0;
        }

        @Override
        public double getAngularVelocity() {
            throw new UnsupportedOperationException();
        }

        @Override
        public double getAngularAcceleration() {
            throw new UnsupportedOperationException();
        }

        @Override
        public double getSlipVelocity() {
            throw new UnsupportedOperationException();
        }

        @Override
        public double getSlipAcceleration() {
            throw new UnsupportedOperationException();
        }

        @Override
        public double getSpeed() {
            return 0;
        }

        @Override
        public double getLeverLength() {
            return 0;
        }

        @Override
        public double getInPieceDistance() {
            return inPieceDistance;
        }

        @Override
        public int getPieceIndex() {
            return 0;
        }

        @Override
        public void setThrottle(double throttle) {
        }
    }

    private class MockTrack implements Track {

        public MockCar car;

        @Override
        public void initMyCar(Object myCar) {

        }

        @Override
        public void initTrack(Object trackInfo) {

        }

        @Override
        public void updatePositions(Object positions) {

        }

        @Override
        public double myDistanceToNextBend(int laneIndex) {
            return 0;
        }

        @Override
        public double myDistanceToNextSwitch(int laneIndex) {
            return 0;
        }

        @Override
        public double myDistanceToSecondNextSwitch(int laneIndex) {
            return 0;
        }

        @Override
        public double myBendRadius(int laneIndex) {
            return 0;
        }

        @Override
        public double myNextBendRadius(int laneIndex) {
            return 0;
        }

        @Override
        public double myBendAngle() {
            return 0;
        }

        @Override
        public double myNextBendAngle() {
            return 0;
        }

        @Override
        public Car getMyCar() {
            return car;
        }

        @Override
        public int getNumberOfLanes() {
            return 0;
        }

        @Override
        public int getMaxDistancePieceIndex() {
            return 0;
        }

        @Override
        public TrackPiece[] nextPieces(int count) {
            return new TrackPiece[0];
        }

        @Override
        public TrackPiece[] nextPieces(int start, int count) {
            return new TrackPiece[0];
        }

        @Override
        public int pieceCount() {
            return 0;
        }

        @Override
        public Car[] getCars() {
            return new Car[0];
        }
    }
}
