package com.helloworldopen.crashtestpod;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ShortestLaneStrategyTest {

    Gson gson = new Gson();
    Track track;
    String initMsg = "{\"msgType\":\"gameInit\",\"data\":{\"race\":{\"track\":{\"id\":\"keimola\",\"name\":\"Keimola\",\"pieces\":[{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":62.0},{\"radius\":100,\"angle\":-45.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":100.0,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":90.0}],\"lanes\":[{\"distanceFromCenter\":-10,\"index\":0},{\"distanceFromCenter\":10,\"index\":1}],\"startingPoint\":{\"position\":{\"x\":-300.0,\"y\":-44.0},\"angle\":90.0}},\"cars\":[{\"id\":{\"name\":\"Crash Test Pod\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":3,\"maxLapTimeMs\":60000,\"quickRace\":true}}},\"gameId\":\"d81e8e66-8ddf-4bcf-8b50-b1843d15b872\"}";
    String myCarMsg = "{\"msgType\":\"yourCar\",\"data\":{\"name\":\"Crash Test Pod\",\"color\":\"red\"},\"gameId\":\"d81e8e66-8ddf-4bcf-8b50-b1843d15b872\"}";
    ShortestLaneStrategy laneStrategy;

    @Before
    public void setUp() throws Exception {
        track = new BasicTrack();
        track.initMyCar(gson.fromJson(myCarMsg, JsonObject.class));
        track.initTrack(gson.fromJson(initMsg, JsonObject.class));
        laneStrategy = new ShortestLaneStrategy(track);
    }

    @After
    public void tearDown() throws Exception {
        track = null;
    }

    @Test
    public void testFullTrack() throws Exception {
        // switch indexes: 3, 8, 13, 18, 25, 29, 35

        // calculating the best lane for before switch considering the whole track and optimal route
        int result[];

        result = laneStrategy.whenAndWhichWay(1, 0, 40 - 1);
        assertEquals(3, result[0]);
        assertEquals(1, result[1]);

        result = laneStrategy.whenAndWhichWay(4, 1, 40 - 4);
        assertEquals(8, result[0]);
        assertEquals(-1, result[1]);

        result = laneStrategy.whenAndWhichWay(10, 0, 40 - 10);
        assertEquals(13, result[0]);
        assertEquals(0, result[1]);

        result = laneStrategy.whenAndWhichWay(14, 0, 40 - 14);
        assertEquals(18, result[0]);
        assertEquals(1, result[1]);

        result = laneStrategy.whenAndWhichWay(19, 1, 40 - 19);
        assertEquals(25, result[0]);
        assertEquals(0, result[1]);

        result = laneStrategy.whenAndWhichWay(26, 1, 40 - 26);
        assertEquals(29, result[0]);
        assertEquals(0, result[1]);

        result = laneStrategy.whenAndWhichWay(30, 1, 40 - 30);
        assertEquals(35, result[0]);
        assertEquals(0, result[1]);
    }


    @Test
    public void testBothLanes() throws Exception {
        // i'm on the left and straight is faster
        int[] result = laneStrategy.whenAndWhichWay(4, 0, 14);
        assertEquals(8, result[0]);
        assertEquals(0, result[1]);

        // i'm on the right but left is faster
        result = laneStrategy.whenAndWhichWay(4, 1, 14);
        assertEquals(8, result[0]);
        assertEquals(-1, result[1]);
    }

    @Test
    public void imOnTheSwitch() throws Exception {
        // piece 3 has switch and right(first) lane is shorter but we can not go there because we are already on the switch
        int[] result = laneStrategy.whenAndWhichWay(3, 0, 4);
        assertEquals(-1, result[0]);
        assertEquals(-2, result[1]);
    }

}