package noobbot;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.helloworldopen.crashtestpod.BasicTrack;
import com.helloworldopen.crashtestpod.Copilot;
import com.helloworldopen.crashtestpod.copilots.CombinedCopilod;

import java.io.*;
import java.net.Socket;

public class Main {
    private static final boolean STOP_AT_CRASH = false;
    public static String trackName = "suzuka";
    public static String password = "abc123";
    public static int carCount = 1;


    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        System.out.println("Created Socket");
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
        System.out.println("Created OutputStream");
        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
        System.out.println("Created InputStream");

        // Join default race
        //new Main(reader, writer, new Join(botName, botKey), null);

        // Create race
//        new Main(reader, writer, null, new CreateRace(botName, botKey));

        // Join created race
        new Main(reader, writer, new JoinRace(botName, botKey), null);
    }

    final Gson gson = new Gson();
    private PrintWriter writer;


    public Main(final BufferedReader reader, final PrintWriter writer, final Join join, CreateRace createRace) throws IOException {
        this.writer = writer;
        String line;

        if (createRace != null) {
            System.out.println("Sending create race!");
            send(createRace);
        } else {
            System.out.println("Sending join! ");
            send(join);
        }

        BasicTrack track = new BasicTrack();

        Copilot copilot = new CombinedCopilod();
//        copilot = new FrictionDetector();
//        copilot = new ConstantSpeedCopilot();
//        copilot = new PredictiveCopilot();
//        copilot = new StupidCopilot();

        int ticks = 0;
        long startTime;
        while ((line = reader.readLine()) != null) {
//            System.out.println(line);
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            startTime = System.currentTimeMillis();
            try {
                switch (msgFromServer.msgType) {
                    case "carPositions":
                        track.updatePositions(gson.fromJson(line, JsonObject.class));
                        int direction = copilot.suggestDirection();
                        if (copilot.suggestActivateTurbo()) {
                            copilot.turboActivated();
                            send(new Turbo("We leave in peace!"));
                        } else if (0 == direction) {
                            send(new Throttle(copilot.suggestThrottle()));
                        } else {
                            //System.out.printf("Sending switchLane: %d\n", direction);
                            copilot.switchLane(direction);
                            send(new SwitchLane(direction));
                        }
                        break;
                    case "join":
                        System.out.println("Joined");
                        break;
                    case "crash":
                        System.out.println("=======Crash=====");
                        System.out.println(line);
                        try {
                            LinkedTreeMap map = (LinkedTreeMap) msgFromServer.data;
                            System.out.println(map);
                            copilot.crashed((String) map.get("color"));
                        } catch (Exception e) {
                            System.err.println(e.getMessage());
                        }

                        if (STOP_AT_CRASH)
                            return;
                        else
                            break;
                    case "yourCar":
                        track.initMyCar(gson.fromJson(line, JsonObject.class));
                        System.out.println("Joined");
                        break;
                    case "gameInit":
                        track.initTrack(gson.fromJson(line, JsonObject.class));
                        copilot.setTrack(track);
                        System.out.println("Race init");
                        break;
                    case "gameEnd":
                        System.out.println("Race end");
                        break;
                    case "gameStart":
                        System.out.println("Race start");
                        send(new Ping());
                        break;
                    case "turboAvailable":
                        System.out.println(line);
                        ThrottleInfo ti = new ThrottleInfo();
                        try {
                            LinkedTreeMap map = (LinkedTreeMap) msgFromServer.data;
                            ti.turboDurationTicks = (int) ((double) map.get("turboDurationTicks"));
                            ti.turboFactor = (double) map.get("turboFactor");
                        } catch (Exception e) {
                            System.err.println(e.getMessage());
                            ti.turboDurationTicks = 30;
                            ti.turboFactor = 3;
                        }

                        copilot.turboAvailable(ti);
                        send(new Ping());
                        break;
                    case "lapFinished":
                        try {
                            LinkedTreeMap map = (LinkedTreeMap) msgFromServer.data;
                            System.out.println(map);
                            String color = (String) ((LinkedTreeMap) map.get("car")).get("color");
                            copilot.lapFinished(color);
                        } catch (Exception e) {
                            System.err.println(e.getMessage());
                        }
                    default:
                        System.out.println(line);
                        send(new Ping());
                        break;
                }

            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class CreateRace extends SendMsg {
    public final String name;
    public final String key;


    CreateRace(String name, String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "createRace";
    }

    @Override
    public String toJson() {
        return "{\"msgType\": \"createRace\", \"data\": {" +
                "  \"botId\": {" +
                "    \"name\": \"" + name + "\"," +
                "    \"key\": \"" + key + "\"" +
                "  }," +
                "  \"trackName\": \"" + Main.trackName + "\"," +
                "  \"password\": \"" + Main.password + "\"," +
                "  \"carCount\": " + Main.carCount +
                "}}";
    }
}

class JoinRace extends Join {
    JoinRace(String name, String key) {
        super(name, key);
    }

    @Override
    public String toJson() {
        return "{\"msgType\": \"joinRace\", \"data\": {" +
                "  \"botId\": {" +
                "    \"name\": \"" + name + "\"," +
                "    \"key\": \"" + key + "\"" +
                "  }," +
                "  \"trackName\": \"" + Main.trackName + "\"," +
                "  \"password\": \"" + Main.password + "\"," +
                "  \"carCount\": " + Main.carCount +
                "}}";
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Turbo extends SendMsg {
    private String msg;

    Turbo(final String msg) {
        this.msg = msg;
    }

    @Override
    protected String msgType() {
        return "turbo";
    }

    @Override
    protected Object msgData() {
        return msg;
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class SwitchLane extends SendMsg {

    private int direction;

    SwitchLane(int direction) {
        this.direction = direction;
    }

    @Override
    protected Object msgData() {
        return direction == -1 ? "Left" : "Right";
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}