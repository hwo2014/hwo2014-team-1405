package com.helloworldopen.crashtestpod;

import java.util.LinkedList;

/**
 * Created by Rene Raasuke on 27.04.2014.
 */
public class OvertakeStrategy {

    private final Track track;
    private TrackPiece[] switches;

    public OvertakeStrategy(Track track) {
        this.track = track;
        LinkedList<TrackPiece> l = new LinkedList<TrackPiece>();
        for (TrackPiece piece : track.nextPieces(0, track.pieceCount())) {
            if (piece.hasSwitch()) {
                l.add(piece);
            }
        }
        switches = new TrackPiece[l.size()];
        switches = l.toArray(switches);
    }

    /**
     * Used for sorting the output of getLaneWeights
     *
     * @return int[] directions sorted by weight
     */
    public int[] sortCandidateIndexes(int[] candidates) {
        int[] candidateOrder = new int[]{0, 1, 2};
        int tmp;

        if (candidates[candidateOrder[2]] > candidates[candidateOrder[1]]) {
            tmp = candidateOrder[1];
            candidateOrder[1] = candidateOrder[2];
            candidateOrder[2] = tmp;
        }

        if (candidates[candidateOrder[1]] > candidates[candidateOrder[0]]) {
            tmp = candidateOrder[0];
            candidateOrder[0] = candidateOrder[1];
            candidateOrder[1] = tmp;
        }

        if (candidates[candidateOrder[2]] > candidates[candidateOrder[1]]) {
            tmp = candidateOrder[1];
            candidateOrder[1] = candidateOrder[2];
            candidateOrder[2] = tmp;
        }
        return candidateOrder;
    }

    /**
     * -2    - it's a wall
     * -1    - has awfully slow car
     * 0-999 - slower car - speed of the car * 10
     * 1000  - free lane
     * 1001+ - faster car - must be good lane :)
     *
     * @return array of 3: integers [left, straight, right] with respective weights
     */

    public int[] getLaneWeights(Car[] blockingCars) {
        Car myCar = track.getMyCar();


        int[] candidatePreference = new int[]{0, 0, 0};

        // when is a car considered faster
        // it might be a good car but just slowing down
        double referenceSpeed = myCar.getSpeed();

        // calculate the weights for each lane
        for (int i = 0; i < blockingCars.length; i++) {
            Car blockingCar = blockingCars[i];
            int resultingLane = myCar.getEndLane() + i - 1;
            if (blockingCar == null) {
                if (resultingLane < 0 || resultingLane >= track.getNumberOfLanes()) {
                    // can not switch there
                    candidatePreference[i] = -2;
                } else {
                    // it's a free lane
                    candidatePreference[i] = 1000;
                }
            } else {
                if (blockingCar.getSpeed() < referenceSpeed / 2) {
                    // awfully slow
                    candidatePreference[i] = -1;
                } else if (blockingCar.getSpeed() < referenceSpeed) {
                    // it's moving but slower than us
                    candidatePreference[i] = (int) (blockingCar.getSpeed() * 10);
                } else {
                    // it's faster - it must know where it's going or is going to fly off soon so no worries
                    candidatePreference[i] = 1001 + (int) (blockingCar.getSpeed() * 10);
                }
            }
        }
        return candidatePreference;
    }

    /**
     * Returns slowest car between upcoming two switches per each lane. Go ahead and pick one from the free or fast lanes.
     *
     * @return Car[] {left, current, right} null if there is no car on the lane or you can not switch in that direction
     */

    public Car[] getBlockingCars() {
        Car[] otherCars = track.getCars();
        Car myCar = track.getMyCar();
        TrackPiece firstSwitch;
        TrackPiece secondSwitch;
        int firstSwitchIndex;
        int secondSwitchIndex;

        // find first two upcoming switches
        int closest = 0;
        int closestSwitchDistance = Integer.MAX_VALUE;
        int myCarIndex = myCar.getPieceIndex();

        for (int i = 0; i < switches.length; i++) {
            int switchIndex = switches[i].getIndex();
            int diff = switchIndex - myCarIndex;
            if (switchIndex - myCarIndex < 0) {
                diff = diff + track.pieceCount();
            }
            if (diff < closestSwitchDistance) {
                closestSwitchDistance = diff;
                closest = i;
            }
        }

        firstSwitch = switches[closest];
        firstSwitchIndex = firstSwitch.getIndex();
        secondSwitch = switches[(closest + 1) % switches.length];
        secondSwitchIndex = secondSwitch.getIndex();


        // find the slowest cars between the first switch (included) and second switch (included)
        Car[] slowestCarOnLane = new Car[track.getNumberOfLanes()];
        for (Car otherCar : otherCars) {
            if (otherCar == myCar) {
                continue;
            }
            int otherCarIndex = otherCar.getPieceIndex();
            int lane = otherCar.getEndLane();

            if (firstSwitchIndex < secondSwitchIndex) {
                if (otherCarIndex >= firstSwitchIndex && otherCarIndex <= secondSwitchIndex) {
                    if (slowestCarOnLane[lane] == null || slowestCarOnLane[lane].getSpeed() > otherCar.getSpeed()) {
                        slowestCarOnLane[lane] = otherCar;
                    }
                }
            } else {
                // first switch is in the end of lap and second is in the beginning (of next lap)
                if (otherCarIndex >= firstSwitchIndex || otherCarIndex <= secondSwitchIndex) {
                    if (slowestCarOnLane[lane] == null || slowestCarOnLane[lane].getSpeed() > otherCar.getSpeed()) {
                        slowestCarOnLane[lane] = otherCar;
                    }
                }
            }
        }

        // fill the result with cars if they exist, NULL if not.
        int myLane = myCar.getEndLane();
        Car[] result = new Car[3];
        // if can switch left add a car on the left lane
        if (myLane - 1 >= 0) {
            result[0] = slowestCarOnLane[myLane - 1];
        }
        // on the current lane
        result[1] = slowestCarOnLane[myLane];

        // if can switch right
        if (myLane + 1 < track.getNumberOfLanes()) {
            result[2] = slowestCarOnLane[myLane + 1];
        }

        return result;
    }
}
