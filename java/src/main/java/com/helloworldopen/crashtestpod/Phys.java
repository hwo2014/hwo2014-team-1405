package com.helloworldopen.crashtestpod;

import static java.lang.Math.*;

/**
 *
 */
public class Phys {
    //<irony>Long live global variables </irony>
    public static double grip = 0.311896668;
    public static double drag = 0.02;
    public static double enginePower = 0.2;

    public static double centripetalAcceleration(double speed, double radius) {
        return speed * speed / radius;
    }

    public static double force(double mass, double acceleration) {
        return mass * acceleration;
    }

    public static double curveSpeed(double centripetalForce, double radius) {
        return sqrt(centripetalForce * radius);
    }

    public static double slipAngle(double forwardSpeed, double sideSpeed) {
        return -atan(sideSpeed / abs(forwardSpeed));
    }

    public static double slipSideSpeed(double forwardSpeed, double slipAngle) {
        return tan(-slipAngle) * abs(forwardSpeed);
    }

    public static double slipForwardSpeed(double sideSpeed, double slipAngle) {
        return sideSpeed / tan(-slipAngle);
    }

    public static double angularAcceleration(double angle, double joonKiirus, double frictionForce, double dampeningCoefficient) {
        return -sin(angle) * frictionForce - joonKiirus * dampeningCoefficient;
    }

    public static double dampeningCoefficient(double angle, double joonKiirus, double frictionForce, double angularAcceleration) {
        return (sin(angle) * frictionForce + angularAcceleration) / (-joonKiirus);
    }

    public static double frictionForce(double angle, double joonKiirus, double dampeningCoefficient, double angularAcceleration) {
        return (joonKiirus * dampeningCoefficient + angularAcceleration) / (-sin(angle));

    }

    public static double joonKiirus(double angleDelta, double radius) {
        return angleDelta * radius;
    }

    public static double[] predictNextTick(double speed, double throttle, double radius, int bendDirection, double angle, double angularVelocity, double turboFactor, double carRadius) {

        double grip = Math.abs(Phys.grip);
        double drag = Phys.drag;
        double d = 0.1;
        double enginePower = Phys.enginePower * turboFactor;
        double newAngularVelocity;

        double acceleration = throttle * enginePower;
        double newSpeed = speed * (1 - drag) + acceleration;

        double a = calculateSlipAcceleration(speed, radius, bendDirection, grip);
        double dampening = calculateDampening(speed, angle, angularVelocity, d, carRadius, acceleration);

        newAngularVelocity = angularVelocity + (a + dampening) / carRadius;
        return new double[]{newSpeed, angle + newAngularVelocity, newAngularVelocity};
    }

    public static double calculateSlipAcceleration(double speed, double radius, int bendDirection, double grip) {
        double cpa = 0;
        if (radius > 0) {
            cpa = speed * speed / radius;
        }
        double a = 0;
        if (cpa > grip) {
            a = bendDirection * (cpa - grip);
        }
        return a;
    }

    public static double calculateDampening(double speed, double angle, double angularVelocity, double d, double carRadius, double acceleration) {
        return -Math.sin(angle) * acceleration - angularVelocity * carRadius * d - speed * Math.sin(angle) * 0.004;
    }

    public static double roughBendSpeed(double radius) {
        if (radius == 0) {
            return Double.MAX_VALUE;
        }
        double d = 0.1;
        double carRadius = 20;
        double step = .05;

        double estimatedSpeed = sqrt(radius / 2) + 5 * step;
        for (double speed = estimatedSpeed - 10 * step; speed <= estimatedSpeed; speed += step) {
            double sa = calculateSlipAcceleration(speed, radius, 1, Phys.grip);
            double dampening = calculateDampening(speed, 1, 0, d, carRadius, Phys.enginePower);
            if ((sa + dampening) / carRadius > 0)
                return speed - step;
        }
        return estimatedSpeed;
    }

    public static double ticksToBrake(double start, double end) {
        return logOfBase(1 - Phys.drag, end / start);
    }

    public static double distanceTravelledWhileBraking(double start, double end) {
        double ticks = ticksToBrake(start, end);
        double log = Math.log(1 - drag);
        return start * pow(1 - drag, ticks) / log - start / log;
    }

    public static double logOfBase(double base, double num) {
        return Math.log(num) / Math.log(base);
    }

}
