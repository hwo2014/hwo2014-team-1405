package com.helloworldopen.crashtestpod.copilots;

import com.helloworldopen.crashtestpod.Car;
import com.helloworldopen.crashtestpod.Copilot;
import com.helloworldopen.crashtestpod.Track;
import com.helloworldopen.crashtestpod.helpers.AccelerationCalculator;
import com.helloworldopen.crashtestpod.helpers.Telemetry;
import noobbot.ThrottleInfo;

import static java.lang.Math.*;

/**
 * Stupid copilot will take only traction in account
 */
public class StupidCopilot implements Copilot {
    private AccelerationCalculator calculator = new AccelerationCalculator();
    private SpeedModel esm;
    private Car car;
    private Track track;
    private Telemetry t;
    private int previousPieceIndex;
    private int switchDirection = 0;
    private String cause;
    private double prevSlipAngleRad = 0;
    private double prevCarSpeed = Double.MIN_VALUE;
    private int turboActive = 0;

    private boolean spike = false;
    private ThrottleInfo ti = null;

    @Override
    public double suggestThrottle() {
        t.update();
        cause = "";
        updateWithoutThrottle();

        double slipAngleRad = Math.toRadians(car.getSlipAngle());
        double angularVelocity = slipAngleRad - prevSlipAngleRad;

        double carSpeed = removeUnnaturalSpikes();
        double throttle = esm.giveSafeThrottleFor(slipAngleRad, angularVelocity, switchDirection, carSpeed, max(turboActive--, 0), ti != null ? ti.turboFactor : 3);

        cause += switchDirection + "  " + car.getStartLane() + "|" + car.getEndLane();
        //logging(throttle);

        prevSlipAngleRad = slipAngleRad;

        return max(0, min(throttle, 1));
    }

    public void updateWithoutThrottle() {
        if (previousPieceIndex != car.getPieceIndex() && track.nextPieces(1)[0].hasSwitch()) {
            switchDirection = 0;
        }
        previousPieceIndex = car.getPieceIndex();
    }

    private double removeUnnaturalSpikes() {
        double carSpeed;
        if (!spike && prevCarSpeed != Double.MIN_VALUE && abs(car.getSpeed() - prevCarSpeed) > 1) {
            //System.out.println("Speed delta too big!");
            carSpeed = prevCarSpeed;
            spike = true;
        } else {
            carSpeed = car.getSpeed();
            prevCarSpeed = carSpeed;
            spike = false;
        }
        return carSpeed;
    }

    public void setTrack(Track track) {
        this.track = track;
        car = track.getMyCar();
        t = new Telemetry(track, calculator);
        esm = new SpeedModel(track, 50);
    }

    @Override
    public void switchLane(int switchDirection) {
        this.switchDirection = switchDirection;
    }

    @Override
    public void crashed(String color) {
        prevCarSpeed = Double.MIN_VALUE;
    }

    @Override
    public void turboAvailable(ThrottleInfo ti) {
        this.ti = ti;
    }

    @Override
    public void turboActivated() {
        turboActive = ti.turboDurationTicks;
    }

    @Override
    public void lapFinished(String color) {

    }

    private void logging(double throttle) {
        t.log(cause, throttle);
    }

    @Override
    public int suggestDirection() {
        return 0;
    }

    @Override
    public boolean suggestActivateTurbo() {
        return false;
    }

    public void setTelemetry(Telemetry telemetry) {
        this.t = telemetry;
    }

    public void setCalculator(AccelerationCalculator calculator) {
        this.calculator = calculator;
    }
}
