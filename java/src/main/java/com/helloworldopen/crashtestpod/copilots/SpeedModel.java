package com.helloworldopen.crashtestpod.copilots;

import com.helloworldopen.crashtestpod.Car;
import com.helloworldopen.crashtestpod.Track;
import com.helloworldopen.crashtestpod.TrackPiece;

import static com.helloworldopen.crashtestpod.Phys.*;
import static java.lang.Math.*;

class SpeedModel {

    public static final double MAX_SLIP_ANGLE = .95;
    private static final double MAX_ANGULAR_SPEED = 0.1;
    private Track track;
    private int ticksToPredict;
    private final double[] ethrottle;
    private final double[] eslip;
    private final double[] espeed;
    private final double[] eangular;
    private Car car;

    private int direction;
    private double turboFactor = 1;
    private int turboTicks = 0;
    public static final int PIECES_COUNT = 10;

    public SpeedModel(Track track, int ticksToPredict) {
        this.track = track;
        this.ticksToPredict = ticksToPredict;
        car = track.getMyCar();
        eangular = new double[ticksToPredict + 1];
        eslip = new double[ticksToPredict + 1];
        espeed = new double[ticksToPredict + 1];
        ethrottle = new double[ticksToPredict + 1];
        cleanData(ticksToPredict);

    }

    /**
     * Reset all estimations to default values.
     *
     * @param ticksToPredict
     */
    private void cleanData(int ticksToPredict) {

        for (int i = 0; i < ticksToPredict + 1; i++) {
            ethrottle[i] = 1;
            espeed[i] = 0;
            eslip[i] = 0;
            eangular[i] = 0;
        }
    }


    public double giveSafeThrottleFor(final double slipAngle, final double angularVelocity, final int direction, double carSpeed, int turboTicks, double turboFactor) {
        this.direction = direction;
        this.turboFactor = turboFactor;
        this.turboTicks = turboTicks;

        // Precalculate some information
        TrackPiece[] trackPieces = track.nextPieces(PIECES_COUNT);
        Laneing[] laneing = getLaneing(trackPieces);
        int[] ticksToPiece = ticksToPiece(trackPieces, laneing, carSpeed);
        double[] pieceSpeed = piecesSpeed(trackPieces, laneing);


        // are we somehow faster than we can break to some bend than we expected.
        for (int i = 0; i < PIECES_COUNT; i++) {
            if (ticksToPiece[i] < ticksToBrake(carSpeed, pieceSpeed[i])) {
                return 0;
            }
        }

        //Reset estimations
        cleanData(ticksToPredict);
        espeed[0] = carSpeed;
        eslip[0] = slipAngle;
        eangular[0] = angularVelocity;


        //Start estimating stuff
        for (int i = 0; i < ticksToPredict; i++) {
            double[] values;
            do {

                values = makePredictions(trackPieces, i, getTurboFactor(i), laneing);
                storePredictions(i, values);
                double slipAngleSign = eslip[i + 1] / abs(eslip[i + 1]);
                double angularSpeed = eangular[i + 1] / slipAngleSign;
                if (abs(eslip[i + 1]) > MAX_SLIP_ANGLE || angularSpeed > MAX_ANGULAR_SPEED) {
                    while (i > 0 && ethrottle[i] <= 0) {
                        i -= 1;
                    }
                    if (i == 0 && ethrottle[i] == 0) {
                        return 0;
                    }
                    i = decreaseThrottle(i, 3);
                    values = makePredictions(trackPieces, i, getTurboFactor(i), laneing);
                    storePredictions(i, values);
                }
            } while (abs(values[1]) > MAX_SLIP_ANGLE && ethrottle[0] <= 1 && ethrottle[0] > 0);

            ethrottle[i] = bound(ethrottle[i]);
        }

        double throttle = ethrottle[0];
        for (int i = 0; i < ticksToPredict; i++) {
            ethrottle[i] = ethrottle[i + 1];
            eslip[i] = eslip[i + 1];
            espeed[i] = espeed[i + 1];
            eangular[i] = eangular[i + 1];
        }
        ethrottle[ticksToPredict] = 1;

        return throttle;
    }

    /**
     * Calculates rough desired bend speed for each piece
     *
     * @param trackPieces
     * @param laneing
     * @return
     */
    private double[] piecesSpeed(TrackPiece[] trackPieces, Laneing[] laneing) {
        double[] pieceSpeed = new double[trackPieces.length];
        pieceSpeed[0] = 0;

        for (int i = 0; i < trackPieces.length; i++) {
            double radius = trackPieces[i].getRadius(laneing[i].entry, laneing[i].exit);
            pieceSpeed[i] = radius != 0 ? roughBendSpeed(radius) : 1000;
        }
        return pieceSpeed;

    }

    /**
     * Calculates distance to each piece if we keep our current speed. Might be useful if we would update these values depending on predictions.
     *
     * @param trackPieces
     * @param laneing
     * @param carSpeed
     * @return
     */
    private int[] ticksToPiece(TrackPiece[] trackPieces, Laneing[] laneing, double carSpeed) {
        int[] ticksToPiece = new int[trackPieces.length];
        ticksToPiece[0] = 0;

        double distance = 0;
        for (int i = 0; i < trackPieces.length - 1; i++) {
            distance += trackPieces[i].getLength(laneing[i].entry, laneing[i].exit);
            ticksToPiece[i + 1] = (int) floor(distance / carSpeed);
        }
        return ticksToPiece;

    }

    /**
     * Makes estimations about what lane the car occupies on what piece.
     *
     * @param pieces
     * @return
     */
    private Laneing[] getLaneing(TrackPiece[] pieces) {
        Laneing[] laneings = new Laneing[pieces.length];

        boolean switched = false;
        for (int i = 0; i < pieces.length; i++) {

            boolean hasSwitch = pieces[i].hasSwitch();

            int entry;
            int exit;

            if (i == 0) {
                entry = car.getStartLane();
                exit = car.getEndLane();
            } else if (switched) {
                entry = car.getEndLane() + direction;
                exit = car.getEndLane() + direction;
            } else if (hasSwitch) {
                entry = car.getEndLane();
                exit = car.getEndLane() + direction;
                switched = true;
            } else {
                entry = car.getEndLane();
                exit = car.getEndLane();
            }
            entry = max(min(track.getNumberOfLanes(), entry), 0);
            exit = max(min(track.getNumberOfLanes(), exit), 0);
            laneings[i] = new Laneing(entry, exit);
        }
        return laneings;
    }


    private double getTurboFactor(int i) {
        return i < turboTicks ? this.turboFactor : 1;
    }

    private int decreaseThrottle(final int tick, int howManyTicks) {
        int newTick = tick;
        for (int i = howManyTicks; i > 0 && newTick >= 0; i--) {
            decreaseThrottle(newTick);
            newTick -= 1;
        }
        return max(0, newTick);
    }

    private void decreaseThrottle(int i) {
        ethrottle[i] = bound(ethrottle[i] - 0.25);
    }

    private double[] makePredictions(TrackPiece[] trackPieces, int i, double turboFactor, Laneing[] laneing) {
        int pieceIndex = getPieceFromDistance(trackPieces, getDistanceTravelled(i), laneing);

        TrackPiece currentPiece = trackPieces[pieceIndex];
        double radius = currentPiece.getRadius(laneing[pieceIndex].entry, laneing[pieceIndex].exit);
        double angle = currentPiece.getBendAngle();
        double bendDirection = angle != 0 ? angle / abs(angle) : 0;

        return predictNextTick(espeed[i], ethrottle[i], radius, (int) bendDirection, eslip[i], eangular[i], turboFactor, car.getLeverLength());
    }

    private void storePredictions(int i, double[] values) {
        espeed[i + 1] = values[0];
        eslip[i + 1] = values[1];
        eangular[i + 1] = values[2];
    }

    private double getDistanceTravelled(int ticks) {
        double distanceTravelled = car.getInPieceDistance();
        for (int i = 0; i < ticks; i++) {
            distanceTravelled += espeed[i];
        }
        return distanceTravelled;
    }

    private int getPieceFromDistance(final TrackPiece[] pieces, final double distance, Laneing[] laneing) {

        double distRemaining = distance;
        for (int i = 0; i < pieces.length; i++) {

            distRemaining -= pieces[i].getLength(laneing[i].entry, laneing[i].exit);
            if (distRemaining < 0)
                return i;
        }
        return 0;

    }

    public double getDistanceTravelled() {
        return getDistanceTravelled(ticksToPredict);

    }

    private static double bound(double v) {
        return max(min(v, 1), 0);
    }

    public static class Laneing {

        public final int entry;
        public final int exit;

        public Laneing(int entry, int exit) {
            this.entry = entry;
            this.exit = exit;
        }
    }

}
