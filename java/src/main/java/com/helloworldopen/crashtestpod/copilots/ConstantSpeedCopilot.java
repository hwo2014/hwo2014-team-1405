package com.helloworldopen.crashtestpod.copilots;

import com.helloworldopen.crashtestpod.Car;
import com.helloworldopen.crashtestpod.Copilot;
import com.helloworldopen.crashtestpod.Track;
import com.helloworldopen.crashtestpod.helpers.AccelerationCalculator;
import com.helloworldopen.crashtestpod.helpers.Telemetry;
import noobbot.ThrottleInfo;

/**
 *
 */
public class ConstantSpeedCopilot implements Copilot {
    public static final double SPEED = 6.5;
    private AccelerationCalculator calculator = new AccelerationCalculator();
    private Telemetry t;
    private Car car;

    boolean speedAchieved = false;

    @Override
    public double suggestThrottle() {
        t.update();

        double throttle = SPEED / 10;
        if (car.getSpeed() < SPEED && !speedAchieved) {
            throttle = 1;
        } else {
            speedAchieved = true;
        }
        t.log("constant speed copilot", throttle);
        return throttle;
    }

    @Override
    public void setTrack(Track track) {
        car = track.getMyCar();
        t = new Telemetry(track, calculator);
    }

    @Override
    public void switchLane(int switchDirection) {

    }

    @Override
    public void crashed(String color) {

    }

    @Override
    public void turboAvailable(ThrottleInfo ti) {

    }

    @Override
    public void turboActivated() {

    }

    @Override
    public void lapFinished(String color) {

    }

    @Override
    public int suggestDirection() {
        return 0;
    }

    @Override
    public boolean suggestActivateTurbo() {
        return false;
    }
}
