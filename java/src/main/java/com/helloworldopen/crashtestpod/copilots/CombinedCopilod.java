package com.helloworldopen.crashtestpod.copilots;

import com.helloworldopen.crashtestpod.*;
import com.helloworldopen.crashtestpod.helpers.AccelerationCalculator;
import com.helloworldopen.crashtestpod.helpers.Telemetry;
import noobbot.ThrottleInfo;

/**
 *
 */
public class CombinedCopilod implements Copilot {


    private StupidCopilot stupidCopilot = new StupidCopilot();
    private PhysicsDetector physicsDetector;
    private Track track;
    private OvertakeStrategy overtakeStrategy;

    private int previousLane = Integer.MIN_VALUE;
    private boolean turbo = false;
    private Telemetry telemetry;
    private boolean physicsDetected = false;
    private LaneStrategy laneStrategy;
    private int lastPieceIndex = -1;

    private int nextSwitchIndex = -2;
    private int nextSwitchOptimalDirection = 0;
    private int nextSwitchPlannedDirection = 0;
    private boolean switchSentForThisPiece = false;
    private int sentSwitchDirection = 0;

    public CombinedCopilod() {
    }

    @Override
    public double suggestThrottle() {
        telemetry.update();
        if (!physicsDetected) {
            // TODO: DISABLE lane switching during detection - this will screw up the detection as we are unable to calculate radius for switches on bends
            stupidCopilot.updateWithoutThrottle();
            double throttle = physicsDetector.suggestThrottle();

            Phys.enginePower = physicsDetector.model.power;
            Phys.drag = physicsDetector.model.drag;
            Phys.grip = physicsDetector.model.grip;

            physicsDetected = Phys.enginePower > 0 && Phys.drag > 0 && Phys.grip > 0;
            return throttle;
        }

        double throttle = stupidCopilot.suggestThrottle();
        //telemetry.log("", throttle);

        track.getMyCar().setThrottle(throttle);
        return throttle;
    }


    @Override
    public void setTrack(Track track) {
        this.track = track;
        physicsDetector = new PhysicsDetector(track);
        stupidCopilot.setTrack(track);
        physicsDetector.setTrack(track);
        AccelerationCalculator calculator = new AccelerationCalculator();
        laneStrategy = new ShortestLaneStrategy(track);

        telemetry = new Telemetry(track, calculator);
        stupidCopilot.setTelemetry(telemetry);
        physicsDetector.setTelemetry(telemetry);
        stupidCopilot.setCalculator(calculator);
        overtakeStrategy = new OvertakeStrategy(track);
    }

    @Override
    public int suggestDirection() {
        if (!physicsDetected)
            return 0;

        Car myCar = track.getMyCar();
        int myCarPieceIndex = myCar.getPieceIndex();

        if (lastPieceIndex != myCarPieceIndex) {
            // on an new piece now
            switchSentForThisPiece = false;
            sentSwitchDirection = 0;
            //System.out.println("Reset direction");
        }


        try { // there is lots of array magic in here and we are less than 99.9999% sure it's all safe - just in case

            Car[] blockingCars = overtakeStrategy.getBlockingCars();
            int[] candidates = overtakeStrategy.getLaneWeights(blockingCars);
            int[] sortedIndexes = overtakeStrategy.sortCandidateIndexes(candidates);
            int firstBestIndex = sortedIndexes[0];
            int secondBestIndex = sortedIndexes[1];
            int thirdBestIndex = sortedIndexes[2];
            //System.out.printf("OTS cars[%s, %s, %s], weights[%d,%d,%d] sorted[%d,%d,%d]\n", blockingCars[0], blockingCars[1], blockingCars[2], candidates[0], candidates[1], candidates[2], sortedIndexes[0], sortedIndexes[1], sortedIndexes[2]);
            if (candidates[firstBestIndex] == candidates[secondBestIndex]) {

                // at least 2 top choices are equal - we will have to consult lane strategy
                if (lastPieceIndex != myCarPieceIndex) {
                    // we have moved to another piece and might have more information - time to recalculate
                    int[] ls = laneStrategy.whenAndWhichWay(myCarPieceIndex, myCar.getEndLane(), track.pieceCount() / 2);
                    nextSwitchIndex = ls[0];
                    nextSwitchOptimalDirection = ls[1];
                }

                // try to take lane preference into account
                if (nextSwitchIndex >= 0 && nextSwitchOptimalDirection >= -1 && nextSwitchOptimalDirection <= 1) {
                    // there is a preferred lane
                    if (nextSwitchOptimalDirection + 1 == firstBestIndex || nextSwitchOptimalDirection + 1 == secondBestIndex) {
                        // preferred lane matches with either of the two best - that's where we go
                        nextSwitchPlannedDirection = nextSwitchOptimalDirection;
                    } else {
                        // it must be the third best a.k.a. worst
                        if (candidates[thirdBestIndex] == candidates[firstBestIndex]) {
                            // third best is actually as good as first two - good to go
                            nextSwitchPlannedDirection = nextSwitchOptimalDirection;
                        } else {
                            // third option is the only bad choice
                            // let's try to pick the one from best options which is closer to the third

                            if (thirdBestIndex == 1) {
                                // if worst is middle both best must be left and right - let's just choose right because ... right
                                nextSwitchPlannedDirection = 1;
                            } else {
                                // worst is left or right - closest is middle
                                nextSwitchPlannedDirection = 0;
                            }
                        }
                    }
                } else {
                    // no lane preference. Let's just pick the top one
                    nextSwitchPlannedDirection = firstBestIndex - 1;
                }

            } else {
                // just choose the least blocked lane
                nextSwitchPlannedDirection = firstBestIndex - 1;
            }
            // inform the other pilot we are planning to switch on the next switch
            stupidCopilot.switchLane(nextSwitchPlannedDirection);

        } catch (RuntimeException e) {
            System.err.println("We caught a nasty exception at suggestDirection but will try to continue");
            e.printStackTrace();
        }

        lastPieceIndex = myCarPieceIndex;

        if ((nextSwitchIndex - myCarPieceIndex) % track.pieceCount() == 1) {
            // next piece is the switch - send the message
            if (switchSentForThisPiece == true) {
                // we already sent it
                if (sentSwitchDirection != nextSwitchPlannedDirection) {
                    //System.out.println("Reconsidered switch");
                    // but we have changed our mind
                    return nextSwitchPlannedDirection;
                }
            } else {
                return nextSwitchPlannedDirection;
            }
        }
        // no need to switch right now - just straight ahead
        return 0;
    }

    @Override
    public boolean suggestActivateTurbo() {
        Car car = track.getMyCar();
        TrackPiece trackPiece = track.nextPieces(1)[0];
        int index = trackPiece.getIndex();
        if (turbo &&
                track.getMaxDistancePieceIndex() - 1 == index &&
                (trackPiece.getLength(car.getEndLane(), car.getEndLane()) - car.getInPieceDistance()) < trackPiece.getRadius(car.getEndLane(), car.getEndLane()) / 3) {
            System.out.println(track.getMaxDistancePieceIndex() + " " + index + " " + car.getPieceIndex());
            turbo = false;
            return true;
        } else return false;
    }

    @Override
    public void switchLane(int switchDirection) {
        this.sentSwitchDirection = switchDirection;
        this.switchSentForThisPiece = true;
        physicsDetector.switchLane(switchDirection);
        stupidCopilot.switchLane(switchDirection);
    }

    @Override
    public void crashed(String color) {
        if (track.getMyCar().getColor().equals(color)) {
            Phys.grip *= 0.95;
            telemetry.setMaxSlipAngle(0.0);
        }
        stupidCopilot.crashed(color);
    }

    @Override
    public void lapFinished(String color) {
        if (telemetry.getMaxSlipAngle() < 50) {
            Phys.grip *= 1.05;
        }
    }

    @Override
    public void turboAvailable(ThrottleInfo ti) {
        stupidCopilot.turboAvailable(ti);
        turbo = true;
    }

    @Override
    public void turboActivated() {
        stupidCopilot.turboActivated();
    }


    public static class ShortestPathCalculator {

    }
}
