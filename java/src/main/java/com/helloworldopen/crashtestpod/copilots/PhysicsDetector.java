package com.helloworldopen.crashtestpod.copilots;

import com.helloworldopen.crashtestpod.Car;
import com.helloworldopen.crashtestpod.Copilot;
import com.helloworldopen.crashtestpod.Track;
import com.helloworldopen.crashtestpod.helpers.Telemetry;
import noobbot.ThrottleInfo;

import static com.helloworldopen.crashtestpod.Phys.*;

/**
 *
 */
public class PhysicsDetector implements Copilot {
    private Track track;
    private Car car;

    private double friction = 0.35;

    public Model model;
    private double lastThrottle = 0;
    private Telemetry telemetry;
    private int previousPieceIndex;
    private int switchDirection;

    public PhysicsDetector(Track track) {
        this.track = track;
        car = track.getMyCar();
    }

    @Override
    public double suggestThrottle() {
        telemetry.update();
        if (previousPieceIndex != car.getPieceIndex() && track.nextPieces(1)[0].hasSwitch()) {
            switchDirection = 0;
        }

        double throttle = 1;
        //System.out.println("==Debug==");
        //System.out.println(car.getDistanceTravelled());
        //System.out.println(car.getSpeed());
        //System.out.println(lastThrottle);
        //System.out.println(car.getSlipAngle() * Math.PI / 180);
        //System.out.println(track.myBendRadius(car.getEndLane()));
        //System.out.println(track.myBendAngle());
        //System.out.println();
        if (model == null) {
            model = new Model(car.getDistanceTravelled());
        }
        model.learn(car.getDistanceTravelled(), car.getSpeed(), lastThrottle, car.getSlipAngle() * Math.PI / 180, track.myBendRadius(car.getEndLane()), track.myBendAngle(), car.getLeverLength());
        if (car.getSpeed() == 0) {
            lastThrottle = throttle;
            telemetry.log("Physics detector - detecting full throttle and getting faster up to speed", throttle);
            return throttle;
        }
        double curveSpeed = curveSpeed(friction, track.myNextBendRadius(car.getEndLane() + switchDirection));
        if (curveSpeed < car.getSpeed()) {
            throttle = 0;
        } else {
            throttle = curveSpeed / 10;
        }

        if (track.myBendRadius(car.getEndLane()) > 0 && car.getSlipAngle() == 0) {
            //full throttle so we would start slipping
            throttle = 1;
            telemetry.log("Physics detector - not slipping yet " + curveSpeed + " " + switchDirection + "  " + car.getStartLane() + "|" + car.getEndLane(), throttle);
        } else {
            telemetry.log("Physics detector - waiting for conditions " + curveSpeed + " " + switchDirection + "  " + car.getStartLane() + "|" + car.getEndLane(), throttle);
        }

        lastThrottle = throttle;
        return throttle;
    }

    public void setTrack(Track track) {
    }

    @Override
    public void switchLane(int switchDirection) {
        this.switchDirection = switchDirection;
    }

    @Override
    public void crashed(String color) {
    }

    @Override
    public void turboAvailable(ThrottleInfo ti) {
    }

    @Override
    public void turboActivated() {

    }

    @Override
    public void lapFinished(String color) {

    }

    @Override
    public int suggestDirection() {
        return 0;
    }

    @Override
    public boolean suggestActivateTurbo() {
        return false;
    }

    public void setTelemetry(Telemetry telemetry) {
        this.telemetry = telemetry;
    }

    public Telemetry getTelemetry() {
        return telemetry;
    }

    public class Model {
        // how much acceleration throttle creates
        public double power = 0;

        // how much drag velocity produces
        public double drag = 0;

        // grip - acceleration greater than this causes slip
        public double grip = 0;

        private double prevDistance = 0;
        private boolean stopped = true;
        private double prevVelocity = 0;
        private double prevSlipAngle = 0;
        private double prevAngularVelocity = 0;

        public Model(double prevDistance) {
            this.prevDistance = prevDistance;
        }

        void learn(double distance, double velocity, double throttle, double slipAngle, double bendRadius, double bendAngle, double leverLength) {

            if (velocity == 0 || prevDistance == distance) {
                stopped = true;
            } else {
                if (stopped) {
                    // was stopped but is moving now
                    // use the chance to get pure acceleration without friction
                    if (throttle > 0) {
                        power = velocity / throttle;
                        //System.out.printf("My acceleration is: %f\n", power);
                    }
                }
                stopped = false;

                drag = Math.abs((velocity - throttle * power) / prevVelocity - 1);

                //System.out.printf("My drag is: %f\n", drag);
            }

            if (Math.abs(slipAngle) > 0 && prevSlipAngle == 0 && prevAngularVelocity == 0) {
                // first slipping point
                // slip speed = slip angle and is only determined by the centripetal acceleration and grip
                double cpa = prevVelocity * prevVelocity / bendRadius;
                grip = cpa - Math.abs(slipAngle * leverLength);
                //System.out.printf("My grip is: %f\n", grip);
            }

            prevDistance = distance;
            prevVelocity = velocity;
            prevAngularVelocity = slipAngle - prevSlipAngle;
            prevSlipAngle = slipAngle;
        }
    }
}
