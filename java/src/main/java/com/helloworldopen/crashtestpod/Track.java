package com.helloworldopen.crashtestpod;

/**
 * Created by Rene Raasuke on 15.04.2014.
 * <p/>
 * Passive model for "the world".
 * Keeps track state and responds to Copilot's questions like how long till the next turn
 */
public interface Track {

    public void initMyCar(Object myCar);

    public void initTrack(Object trackInfo);

    public void updatePositions(Object positions);

    public double myDistanceToNextBend(int laneIndex);

    public double myDistanceToNextSwitch(int laneIndex);

    public double myDistanceToSecondNextSwitch(int laneIndex);

    public double myBendRadius(int laneIndex);

    public double myNextBendRadius(int laneIndex);

    public double myBendAngle();

    public double myNextBendAngle();

    public Car getMyCar();

    int getNumberOfLanes();

    int getMaxDistancePieceIndex();

    public TrackPiece[] nextPieces(int count);

    public TrackPiece[] nextPieces(int start, int count);

    public int pieceCount();

    public Car[] getCars();
}
