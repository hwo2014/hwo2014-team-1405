package com.helloworldopen.crashtestpod;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Rene Raasuke on 15.04.2014.
 */
public class BasicTrack implements Track {

    private static final Logger LOG = LoggerFactory.getLogger(BasicTrack.class);

    private MyPiece[] pieces;
    private int pieceCount = 0;
    private double[] lanes;
    private FullCar[] cars;
    private FullCar myCar;
    private String myColor;

    private double[][] distanceNextBend;
    private double[][] nextBendRadius;
    private double[] nextBendAngle;
    private double[][] distanceNextSwitch;
    private double[][] distanceSecondNextSwitch;
    private int maxDistancePieceIndex;

    @Override
    public void initMyCar(Object myCar) {
        JsonObject jo = (JsonObject) myCar;
        JsonObject data = jo.getAsJsonObject("data");
        this.myColor = data.getAsJsonPrimitive("color").getAsString();
    }

    @Override
    public void initTrack(Object trackInfo) {
        JsonObject jo = (JsonObject) trackInfo;
        JsonObject data = jo.getAsJsonObject("data");
        JsonObject race = data.getAsJsonObject("race");
        JsonObject track = race.getAsJsonObject("track");
        JsonArray pieces = track.getAsJsonArray("pieces");
        JsonArray lanes = track.getAsJsonArray("lanes");
        JsonArray cars = race.getAsJsonArray("cars");

        this.pieces = new MyPiece[pieces.size()];
        this.pieceCount = pieces.size();
        int i = 0;

        this.lanes = new double[lanes.size()];
        for (JsonElement element : lanes) {
            JsonObject lane = element.getAsJsonObject();
            this.lanes[lane.getAsJsonPrimitive("index").getAsInt()] = lane.getAsJsonPrimitive("distanceFromCenter").getAsDouble();
        }

        for (JsonElement element : pieces) {

            MyPiece p = new MyPiece();
            JsonObject piece = element.getAsJsonObject();
            JsonPrimitive length = piece.getAsJsonPrimitive("length");
            p.index = i;
            if (length != null) {
                p.length = length.getAsDouble();
            }

            JsonPrimitive aSwitch = piece.getAsJsonPrimitive("switch");
            if (aSwitch != null) {
                p.hasSwitch = aSwitch.getAsBoolean();
            }

            JsonPrimitive radius = piece.getAsJsonPrimitive("radius");
            if (radius != null) {
                p.radius = radius.getAsDouble();
            }

            JsonPrimitive angle = piece.getAsJsonPrimitive("angle");
            if (angle != null) {
                p.angle = angle.getAsDouble();
            }

            p.calculate(this.lanes);

            this.pieces[i++] = p;
        }


        this.cars = new FullCar[cars.size()];
        int j = 0;
        for (JsonElement element : cars) {
            FullCar fullCar = new FullCar();
            JsonObject car = element.getAsJsonObject();
            JsonObject carId = car.getAsJsonObject("id");

            fullCar.name = carId.getAsJsonPrimitive("name").getAsString();
            fullCar.color = carId.getAsJsonPrimitive("color").getAsString();

            JsonObject carDimensions = car.getAsJsonObject("dimensions");
            fullCar.length = carDimensions.getAsJsonPrimitive("length").getAsDouble();
            fullCar.width = carDimensions.getAsJsonPrimitive("width").getAsDouble();
            fullCar.guideFlagPosition = carDimensions.getAsJsonPrimitive("guideFlagPosition").getAsDouble();

            this.cars[j++] = fullCar;
            if (this.myColor.equals(fullCar.color)) {
                this.myCar = fullCar;
            }
        }

        distanceNextBend = new double[pieceCount][lanes.size()];
        nextBendRadius = new double[pieceCount][lanes.size()];
        distanceNextSwitch = new double[pieceCount][lanes.size()];
        distanceSecondNextSwitch = new double[pieceCount][lanes.size()];
        for (int k = 0; k < pieceCount; k++) {
            for (int l = 0; l < lanes.size(); l++) {
                // these calls have more loops inside
                // this is clear waste of cpu cycles and can be improved a lot
                distanceNextBend[k][l] = calculateDistanceToNextBend(k, l);
                nextBendRadius[k][l] = calculateNextBendRadius(k, l);
                distanceNextSwitch[k][l] = calculateDistanceToNextSwitch(k, l);
                distanceSecondNextSwitch[k][l] = calculateDistanceToSecondNextSwitch(k, l);
            }
        }

        maxDistancePieceIndex = -1;
        double maxDistance = 0;
        nextBendAngle = new double[pieceCount];
        for (int k = 0; k < pieceCount; k++) {
            if (this.pieces[k].getBendAngle() == 0) {
                if (distanceNextBend[k][0] > maxDistance) {
                    maxDistance = distanceNextBend[k][0];
                    maxDistancePieceIndex = k;
                }
            }
            nextBendAngle[k] = calculateNextBendAngle(k);
        }
    }

    @Override
    public void updatePositions(Object positions) {
        JsonObject jo = (JsonObject) positions;
        JsonArray cars = jo.getAsJsonArray("data");
        String color;
        int pieceIndex;
        double inPieceDistance;
        int startLaneIndex;
        int endLaneIndex;
        int lap;
        double distanceTravelled;
        double slipAngle;

        for (JsonElement element : cars) {
            JsonObject car = element.getAsJsonObject();
            JsonObject carId = car.getAsJsonObject("id");
            color = carId.getAsJsonPrimitive("color").getAsString();
            for (FullCar fullCar : this.cars) {
                if (fullCar.color.equals(color)) {
                    slipAngle = car.getAsJsonPrimitive("angle").getAsDouble();
                    JsonObject pos = car.getAsJsonObject("piecePosition");
                    pieceIndex = pos.getAsJsonPrimitive("pieceIndex").getAsInt();
                    inPieceDistance = pos.getAsJsonPrimitive("inPieceDistance").getAsDouble();
                    JsonObject lane = pos.getAsJsonObject("lane");
                    startLaneIndex = lane.getAsJsonPrimitive("startLaneIndex").getAsInt();
                    endLaneIndex = lane.getAsJsonPrimitive("endLaneIndex").getAsInt();
                    lap = pos.getAsJsonPrimitive("lap").getAsInt();

                    if (fullCar.pieceIndex != pieceIndex) {
                        // on a new piece - distance travelled can not be reliably calculated as there appears to be slight randomness in piece length
                        // let's try to predict instead
                        if (fullCar == myCar) {
                            // we know our throttle
                            distanceTravelled = fullCar.throttle * Phys.enginePower + fullCar.speed * (1 - Phys.drag);
                        } else {
                            // for others let's just assume their speed did not change
                            distanceTravelled = fullCar.speed;
                        }
                    } else {
                        distanceTravelled = inPieceDistance - fullCar.inPieceDistance;
                    }
                    //distanceTravelled = calculateDistanceBetween(fullCar.lap, fullCar.pieceIndex, fullCar.inPieceDistance, lap, pieceIndex, inPieceDistance, startLaneIndex);
                    fullCar.acceleration = distanceTravelled - fullCar.speed;
                    fullCar.speed = distanceTravelled;
                    fullCar.distanceTravelled += distanceTravelled;

                    // angle calculations
                    fullCar.angularAcceleration = slipAngle - fullCar.angle - fullCar.angularVelocity;
                    fullCar.angularVelocity = slipAngle - fullCar.angle;
                    fullCar.angle = slipAngle;

                    fullCar.updatePosition(pieceIndex, inPieceDistance, startLaneIndex, endLaneIndex, lap);

                    // physics
                    System.out.printf("%4.12f %4.12f %4.12f ", Phys.enginePower, Phys.drag, Phys.grip);
                    // track
                    System.out.printf("%4.12f %4.12f ", pieces[fullCar.pieceIndex].getRadius(fullCar.startLaneIndex, fullCar.endLaneIndex), pieces[fullCar.pieceIndex].getBendAngle());
                    // speed
                    System.out.printf("%4.12f %4.12f ", fullCar.speed, fullCar.throttle);
                    // slip
                    System.out.printf("%4.12f %4.12f %4.12f\n", fullCar.angle, fullCar.angularVelocity, fullCar.angularAcceleration);
                }
            }
        }
    }

    @Override
    public double myDistanceToNextBend(int laneIndex) {
        return this.pieces[myCar.pieceIndex].getLength(laneIndex, laneIndex) - myCar.inPieceDistance + distanceNextBend[myCar.pieceIndex][laneIndex];
    }


    @Override
    public double myDistanceToNextSwitch(int laneIndex) {
        return this.pieces[myCar.pieceIndex].getLength(laneIndex, laneIndex) - myCar.inPieceDistance + distanceNextSwitch[myCar.pieceIndex][laneIndex];
    }


    @Override
    public double myDistanceToSecondNextSwitch(int laneIndex) {
        return this.pieces[myCar.pieceIndex].getLength(laneIndex, laneIndex) - myCar.inPieceDistance + distanceSecondNextSwitch[myCar.pieceIndex][laneIndex];
    }

    @Override
    public double myBendRadius(int laneIndex) {
        double laneOffset = this.lanes[laneIndex];
        int current = myCar.pieceIndex;
        if (this.pieces[current].angle > 0) {
            // right turn
            return this.pieces[current].radius - laneOffset;
        } else if (this.pieces[current].angle < 0) {
            // left turn
            return this.pieces[current].radius + laneOffset;
        }
        return 0;
    }

    @Override
    public double myNextBendRadius(int laneIndex) {
        return nextBendRadius[myCar.pieceIndex][laneIndex];
    }


    private double bendAngle(int pieceIndex) {
        return this.pieces[pieceIndex].angle;
    }


    @Override
    public double myBendAngle() {
        return this.pieces[myCar.pieceIndex].angle;
    }

    @Override
    public double myNextBendAngle() {
        return nextBendAngle[myCar.pieceIndex];
    }

    @Override
    public Car getMyCar() {
        return this.myCar;
    }

    @Override
    public int getNumberOfLanes() {
        return lanes.length;
    }

    @Override
    public int getMaxDistancePieceIndex() {
        return maxDistancePieceIndex;
    }

    @Override
    public TrackPiece[] nextPieces(int count) {
        return nextPieces(myCar.pieceIndex, count);
    }

    @Override
    public TrackPiece[] nextPieces(int start, int count) {
        int myPiece = start;
        TrackPiece[] result = new TrackPiece[count];
        for (int i = 0; i < count; i++) {
            result[i] = pieces[(myPiece + i) % this.pieceCount];
        }
        return result;
    }

    @Override
    public int pieceCount() {
        return pieceCount;
    }

    @Override
    public Car[] getCars() {
        return cars;
    }

    private int nextPieceId(int pieceId) {
        return (pieceId + 1) % this.pieceCount;
    }

    private double calculateDistanceToNextBend(final int currentIndex, final int laneIndex) {
        double distance = 0;
        int current = currentIndex;

        boolean straightEncountered = false;

        for (int i = 0; i < this.pieceCount; i++) {
            current = nextPieceId(current);
            straightEncountered |= this.pieces[current].radius == 0;
            if (this.pieces[current].radius != 0 && (this.pieces[current].angle != bendAngle(currentIndex) | straightEncountered)) {
                break;
            }
            distance += this.pieces[current].getLength(laneIndex, laneIndex);
        }
        return distance;
    }

    private double calculateNextBendRadius(final int pieceIndex, final int laneIndex) {
        double laneOffset = this.lanes[laneIndex];
        int current = pieceIndex;
        for (int i = 0; i < this.pieceCount; i++) {
            current = nextPieceId(current);
            boolean differentBend = this.pieces[current].angle != bendAngle(pieceIndex);
            if (this.pieces[current].angle > 0 && differentBend) {
                // right turn
                return this.pieces[current].radius - laneOffset;
            } else if (this.pieces[current].angle < 0 && differentBend) {
                // left turn
                return this.pieces[current].radius + laneOffset;
            }
        }
        // will get here only if there are no bends in track
        // and that means it's all straight
        return 0;
    }

    private double calculateNextBendAngle(int pieceIndex) {
        int current = pieceIndex;

        for (int i = 0; i < this.pieceCount; i++) {
            current = nextPieceId(current);
            if (this.pieces[current].angle != 0 && this.pieces[current].angle != bendAngle(pieceIndex)) {
                return this.pieces[current].angle;
            }
        }
        return 0;
    }

    private double calculateDistanceToNextSwitch(final int pieceIndex, final int laneIndex) {
        int current = pieceIndex;
        double distance = 0;
        for (int i = 0; i < this.pieceCount; i++) {
            current = nextPieceId(current);
            if (this.pieces[current].hasSwitch) {
                return distance;
            }
            distance += this.pieces[current].getLength(laneIndex, laneIndex);
        }

        // no switches
        return 0;
    }

    private double calculateDistanceToSecondNextSwitch(final int pieceIndex, final int laneIndex) {
        int current = pieceIndex;
        double distance = 0;
        boolean firstSwitchEncountered = false;

        for (int i = 0; i < this.pieceCount; i++) {
            current = nextPieceId(current);
            if (this.pieces[current].hasSwitch && firstSwitchEncountered) {
                return distance;
            }
            firstSwitchEncountered |= this.pieces[current].hasSwitch;
            distance += this.pieces[current].getLength(laneIndex, laneIndex);
        }

        // no switches
        return 0;
    }


    /*
     * This calculation is based on the assumption that there is at least one tick per piece.
     * If a car is too fast or piece too short then there is a chance to skip a piece without a tick.
     * It is difficult to be 100% sure about the distance travelled in this case
     * - which lane was it on, was there a switch?
     */
    private double calculateDistanceBetween(int prevLap, int prevPiece, double prevIPD, int lap, int piece, double IPD, int lane) {

        double distance = IPD - prevIPD;

        // on the next piece already?
        if (nextPieceId(prevPiece) == piece) {
            // add the length of previous piece
            distance += this.pieces[prevPiece].getLength(lane, lane);
        }

        return distance;
    }
}

class MyPiece implements TrackPiece {
    int index = 0;
    double length = 0;
    boolean hasSwitch = false;
    double radius = 0;
    double angle = 0;

    double[] laneRadius;
    double[] laneLength;
    double[] laneOffset;

    public static double PI_RAD = Math.PI / 180;

    void calculate(double[] lanes) {
        laneRadius = new double[lanes.length];
        laneLength = new double[lanes.length];
        laneOffset = lanes;

        for (int i = 0; i < lanes.length; i++) {
            if (angle > 0) {
                // right turn
                laneRadius[i] = radius - lanes[i];
                laneLength[i] = laneRadius[i] * angle * PI_RAD;
            } else if (angle < 0) {
                // left turn
                laneRadius[i] = radius + lanes[i];
                laneLength[i] = laneRadius[i] * angle * PI_RAD * -1;
            } else {
                laneRadius[i] = 0;
                laneLength[i] = length;
            }
        }
    }

    @Override
    public double getLength(int startLane, int endLane) {
        if (hasSwitch && startLane != endLane) {
            // approximate length based on trapeziod diagonals
            // works for both straight and bend pieces
            double distance = Math.abs(laneOffset[startLane] - laneOffset[endLane]);
            return Math.sqrt(laneLength[startLane] * laneLength[endLane] + distance * distance);
        }
        return laneLength[startLane];
    }

    @Override
    public double getRadius(int startLane, int endLane) {
        // in case of switch the radius should be bigger than radius of longer lane
        // but I don't know how to calculate that so I just return the bigger of two
        if (laneRadius[startLane] > laneRadius[endLane]) {
            return laneRadius[startLane];
        }
        return laneRadius[endLane];

    }

    @Override
    public double getBendAngle() {
        return angle;
    }

    @Override
    public boolean hasSwitch() {
        return hasSwitch;
    }

    @Override
    public int getIndex() {
        return index;
    }
}

class FullCar implements Car {

    String color;
    String name;
    double length;
    double width;
    double guideFlagPosition;

    double angle;
    double angularVelocity;
    double angularAcceleration;

    double throttle;

    int pieceIndex;
    double inPieceDistance;
    int startLaneIndex;
    int endLaneIndex;
    int lap;

    double distanceTravelled = 0;
    double speed = 0;
    double acceleration = 0;

    void updatePosition(int pieceIndex, double inPieceDistance, int startLaneIndex, int endLaneIndex, int lap) {
        this.pieceIndex = pieceIndex;
        this.inPieceDistance = inPieceDistance;
        this.startLaneIndex = startLaneIndex;
        this.endLaneIndex = endLaneIndex;
        this.lap = lap;
    }


    @Override
    public String getColor() {
        return color;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getDistanceTravelled() {
        return distanceTravelled;
    }

    @Override
    public int getStartLane() {
        return startLaneIndex;
    }

    @Override
    public int getEndLane() {
        return endLaneIndex;
    }

    @Override
    public double getSlipAngle() {
        return angle;
    }

    @Override
    public double getAngularVelocity() {
        return angularVelocity;
    }

    @Override
    public double getAngularAcceleration() {
        return angularAcceleration;
    }

    @Override
    public double getSlipVelocity() {
        return angularVelocity * getLeverLength();
    }

    @Override
    public double getSlipAcceleration() {
        return getAngularAcceleration() * getLeverLength();
    }

    @Override
    public double getSpeed() {
        return speed;
    }

    @Override
    public double getLeverLength() {
        return length - 2 * guideFlagPosition;
    }

    @Override
    public double getInPieceDistance() {
        return inPieceDistance;
    }

    @Override
    public int getPieceIndex() {
        return pieceIndex;
    }

    @Override
    public void setThrottle(double throttle) {
        this.throttle = throttle;
    }


}