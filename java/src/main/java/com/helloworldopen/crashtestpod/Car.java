package com.helloworldopen.crashtestpod;

/**
 * Should return main parameters for the car
 */
public interface Car {


    public String getColor();

    public String getName();

    public double getDistanceTravelled();


    public int getStartLane();
    /**
     * @return lane index
     */
    public int getEndLane();


    public double getSlipAngle();

    public double getAngularVelocity();

    public double getAngularAcceleration();

    public double getSlipVelocity();

    public double getSlipAcceleration();

    /**
     * @return current speed in units per tick
     */
    public double getSpeed();

    double getLeverLength();

    double getInPieceDistance();

    int getPieceIndex();

    void setThrottle(double throttle);
}
