package com.helloworldopen.crashtestpod;

import java.util.ArrayDeque;

/**
 * Created by Rene Raasuke on 27.04.2014.
 */
public class ShortestLaneStrategy implements LaneStrategy {


    private final Track track;

    public ShortestLaneStrategy(Track track) {
        this.track = track;
    }

    @Override
    public int[] whenAndWhichWay(int currentPieceIndex, int startLane, int pieceHorizon) {

        TrackPiece[] pieces = track.nextPieces(currentPieceIndex, pieceHorizon + 1);
        ArrayDeque<WorkPiece> work = new ArrayDeque(pieces.length);
        int numberOfLanes = track.getNumberOfLanes();
        WorkPiece[] bestForLane = new WorkPiece[numberOfLanes];

        double bestDistance = Double.MAX_VALUE;
        int bestInitialDir = 0;
        int firstSwitchIndex = -1;

        double distance = 0;
        int currentLane = startLane;
        int localPieceIndex = 0;
        int initialDir = -2;
        WorkPiece current;
        TrackPiece nextTrackPiece;

        work.add(new WorkPiece(pieces[0], currentLane, currentLane, localPieceIndex, distance, initialDir));

        // branch and bound method to calculate all possible switch combinations
        do {
            // remove workpieces on the same lane that have worse distance
            // first we find out best workpiece for each lane
            do {
                current = work.removeFirst();
                if (bestForLane[current.endLane] == null || bestForLane[current.endLane].totalDistance >= current.totalDistance) {
                    bestForLane[current.endLane] = current;
                }
            } while (!work.isEmpty() && work.peekFirst().localIndex == current.localIndex);

            // next we only add back best for each lane
            boolean setCurrent = true;
            for (int i = 0; i < bestForLane.length; i++) {
                if (bestForLane[i] != null) {
                    if (setCurrent) {
                        // the first item will be set as current
                        current = bestForLane[i];
                        setCurrent = false;
                    } else {
                        // rest will be added back to work queue
                        work.addFirst(bestForLane[i]);
                    }
                }
                bestForLane[i] = null;
            }
            //current = work.removeFirst();


            currentLane = current.endLane;
            localPieceIndex = current.localIndex + 1;

            if (localPieceIndex >= pieces.length) {
                // no more pieces left
                //System.out.printf("Finished with: %d, distance: %f, initial: %d\n", current.index, current.totalDistance, current.initialDirection);
                if (current.totalDistance < bestDistance) {
                    bestDistance = current.totalDistance;
                    bestInitialDir = current.initialDirection;
                }
                continue;
            }

            distance = current.totalDistance;
            initialDir = current.initialDirection;
            nextTrackPiece = pieces[localPieceIndex];

            //System.out.printf("Piece: %d, distance: %f ", current.index, current.totalDistance);
            if (nextTrackPiece.hasSwitch()) {
                if (firstSwitchIndex < 0) {
                    // remember the first switch
                    firstSwitchIndex = nextTrackPiece.getIndex();
                }
                // add new work piece for each possible option
                if (currentLane + 1 < numberOfLanes) {
                    // can switch right
                    work.addLast(new WorkPiece(nextTrackPiece, currentLane, currentLane + 1, localPieceIndex, distance, initialDir == -2 ? 1 : initialDir));
                    //System.out.printf("switch %d, initial: %d\n ", currentLane + 1, initialDir == -2 ? 1 : initialDir);
                }
                if (currentLane - 1 >= 0) {
                    // can switch left
                    work.addLast(new WorkPiece(nextTrackPiece, currentLane, currentLane - 1, localPieceIndex, distance, initialDir == -2 ? -1 : initialDir));
                    //System.out.printf("switch %d, initial: %d\n", currentLane - 1, initialDir == -2 ? -1 : initialDir);
                }
                // and there is always an option to go straight
                work.addLast(new WorkPiece(nextTrackPiece, currentLane, currentLane, localPieceIndex, distance, initialDir == -2 ? 0 : initialDir));
                //System.out.printf("switch %d, initial: %d\n", currentLane, initialDir == -2 ? 0 : initialDir);
            } else {
                // no switch, just continue
                work.addLast(new WorkPiece(nextTrackPiece, currentLane, currentLane, localPieceIndex, distance, initialDir));
                //System.out.printf("current %d, initial: %d\n", currentLane, initialDir);
            }


        } while (work.size() > 0);

        //System.out.printf("Best distance: %f by turning %d", bestDistance, bestInitialDir);
        return new int[]{firstSwitchIndex, bestInitialDir};
    }


}

class WorkPiece {

    public WorkPiece(TrackPiece piece, int startLane, int endLane, int localIndex, double baseDistance, int initialDirection) {
        this.index = piece.getIndex();
        this.startLane = startLane;
        this.endLane = endLane;
        this.localIndex = localIndex;
        this.totalDistance = baseDistance + piece.getLength(startLane, endLane);
        this.hasSwitch = piece.hasSwitch();
        this.initialDirection = initialDirection;

    }

    int localIndex;
    int index;
    int startLane;
    int endLane;
    double totalDistance;
    boolean hasSwitch;
    int initialDirection;

}
