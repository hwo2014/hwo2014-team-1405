package com.helloworldopen.crashtestpod;

/**
 * Created by Rene Raasuke on 24.04.2014.
 */
public interface TrackPiece {

    public double getLength(int startLane, int endLane);

    public double getRadius(int startLane, int endLane);

    public double getBendAngle();

    public boolean hasSwitch();

    public int getIndex();
}
