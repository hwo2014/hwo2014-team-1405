package com.helloworldopen.crashtestpod.helpers;

import com.helloworldopen.crashtestpod.Car;
import com.helloworldopen.crashtestpod.Phys;
import com.helloworldopen.crashtestpod.Track;

import static com.helloworldopen.crashtestpod.Phys.*;
import static java.lang.Math.*;

/**
 *
 */
public class Telemetry {

    private Car car;
    private AccelerationCalculator calculator;
    private double prevSlipAngle;
    private double previousJoonKiirus;
    private double previousAngularVelocity;
    private double previousAngularVelocityDelta;

    private double maxSlipAngle;


    private double angularVelocity;
    private double angularVelocityDelta;
    private double angularVelocityDeltaDelta;
    private double angularSpeed;
    private double angularSpeedDelta;
    private double slipAngleRad;


    private double joonKiirendus;
    private double joonKiirus;

    private double calcFF = 0;
    private double predictedAngle = 0;

    private Track track;

    public Telemetry(Track track, AccelerationCalculator calculator) {
        this.track = track;
        this.car = track.getMyCar();
        this.calculator = calculator;
    }

    public void update() {
        storeHistory();
        calculator.processSpeed(car.getSpeed());
        slipAngleRad = Math.toRadians(car.getSlipAngle());
        angularVelocity = slipAngleRad - prevSlipAngle; // rad/time
        angularVelocityDelta = angularVelocity - previousAngularVelocity; // rad/time
        angularVelocityDeltaDelta = angularVelocityDelta - previousAngularVelocityDelta; // rad/time
        angularSpeed = Math.abs(slipAngleRad) - Math.abs(prevSlipAngle);
        angularSpeedDelta = Math.abs(angularVelocity) - Math.abs(previousAngularVelocity);
        joonKiirus = joonKiirus(angularVelocity, car.getLeverLength());
        joonKiirendus = joonKiirus - previousJoonKiirus;

        maxSlipAngle = max(abs(car.getSlipAngle()), maxSlipAngle);
    }


    private void storeHistory() {
        prevSlipAngle = slipAngleRad;
        previousJoonKiirus = joonKiirus;
        previousAngularVelocity = angularVelocity;
        previousAngularVelocityDelta = angularVelocityDelta;
    }

    public void log(String cause, double throttle) {

//        printValue(track.myDistanceToNextSwitch(car.getEndLane()));
//        printValue(track.myDistanceToSecondNextSwitch(car.getEndLane()));


        printValue(track.myBendRadius(car.getEndLane()));
        printValue(throttle);
        printValue(car.getSpeed());
        printValue(car.getSlipAngle());


        printValue(Phys.enginePower);
        printValue(Phys.drag);
        printValue(Phys.grip);
        printValue(maxSlipAngle);
//        printValue(slipAngleRad);
//        printValue(angularVelocity);
//        printValue(angularVelocityDelta);

//        printValue(toDegrees(predictedAngle));

        System.out.println(" " + cause);

    }

    private void printValue(double value) {
        System.out.printf("%10.4f,", value);
    }

    public double getAangularVelocity() {
        return angularVelocity;
    }

    public double getSlipAngleRad() {
        return slipAngleRad;
    }

    public double getMaxSlipAngle() {
        return maxSlipAngle;
    }

    public void setMaxSlipAngle(double maxSlipAngle) {
        this.maxSlipAngle = maxSlipAngle;
    }
}
