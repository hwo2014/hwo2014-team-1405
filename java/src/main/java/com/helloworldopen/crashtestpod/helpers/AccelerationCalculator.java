package com.helloworldopen.crashtestpod.helpers;

/**
 *
 */
public class AccelerationCalculator {
    private double currentSpeed = 0;
    private double previous = 0;
    private double delta = 0;

    public void processSpeed(double speed) {
        previous = speed;
        currentSpeed = speed;
        delta = speed - previous;
    }

    public double ticksToSpeed(double speed) {
        double acc = (getAccel(speed) + getAccel(currentSpeed)) / 2;
        if (speed > currentSpeed) {
            return (speed - currentSpeed) / acc;
        }
        return (currentSpeed - speed) / (.2 - acc);
    }

    public double getAccel(double speed) {
        return .2 - speed / 50;
    }

    public double getPrevious() {
        return previous;
    }


    public double getDelta() {
        return delta;
    }

}
