package com.helloworldopen.crashtestpod.helpers;

/**
 * Created by Rene Raasuke on 17.04.2014.
 */
public class SpeedCalculator {

    public static void main(String[] args) {
    }

    public static double maxSpeed(double accel, double friction) {
        return accel / friction;
    }

    public static double accelerationDistance(double v0, double accel, double friction, double v1) {
        double distance = 0;
        double v = v0;

        if (accel <= 0) {
            return Double.MAX_VALUE;
        }

        if (v1 >= maxSpeed(accel, friction)) {
            // infinity
            return Double.MAX_VALUE;
        }

        if (v1 < v0) {
            //never
            return Double.MAX_VALUE;
        }

        for (int t = 0; t < 1000; t++) {
            v = v + accel - v * friction;
            distance += v;
            if (v >= v1) {
                return distance;
            }
        }
        return Double.MAX_VALUE;
    }

    public static double decelerationDistance(double v0, double decel, double friction, double v1) {
        double distance = 0;
        double v = v0;

        if (decel >= 0) {
            return Double.MAX_VALUE;
        }

        if (v1 > v0) {
            //never
            return Double.MAX_VALUE;
        }

        for (int t = 0; t < 1000; t++) {
            v = v + decel - v * friction;
            distance += v;
            if (v <= v1) {
                return distance;
            }
        }
        return Double.MAX_VALUE;
    }
}
