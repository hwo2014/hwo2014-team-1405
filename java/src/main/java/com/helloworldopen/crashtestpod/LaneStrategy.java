package com.helloworldopen.crashtestpod;

/**
 * Created by Rene Raasuke on 27.04.2014.
 */
public interface LaneStrategy {

    /**
     * Tells you when and where to switch.
     *
     * @param startPieceIndex The piece you are on. It does not consider the option of switching on that piece.
     * @param startLane       The lane you are on.
     * @param pieceHorizon    How far to look. 0 means only the switch you are on is checked so start with at least 1 :)
     * @return int[switchIndex, direction]. switchIndex of -1 and direction of -2 means no switches found.
     */
    public int[] whenAndWhichWay(int startPieceIndex, int startLane, int pieceHorizon);

}
