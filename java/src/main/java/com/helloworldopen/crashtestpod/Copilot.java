package com.helloworldopen.crashtestpod;

import noobbot.ThrottleInfo;

/**
 * Created by Rene Raasuke on 15.04.2014.
 * <p>
 * The actual AI. But don't tell that to the pilot ;)
 * Asks information from the Track and makes the decision
 */
public interface Copilot {

    public double suggestThrottle();

    public void setTrack(Track track);

    int suggestDirection();

    boolean suggestActivateTurbo();

    void switchLane(int switchDirection);

    void crashed(String color);

    void turboAvailable(ThrottleInfo ti);

    void turboActivated();

    void lapFinished(String color);
}
